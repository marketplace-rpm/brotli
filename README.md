# Information / Информация

SPEC-файл для создания RPM-пакета **brotli**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/brotli`.
2. Установить пакет: `dnf install brotli`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)